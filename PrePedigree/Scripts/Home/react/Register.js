﻿import React from 'react';
import ReactDOM from 'react-dom';
import '../../../css/base.css';

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentWillMount() {
        console.log('componentWillMount');
    }
    componentDidMount() {

        console.log('componentDidMount');
    }
    componentWillReceiveProps() {
        console.log('componentWillReceiveProps');
    }
    componentWillUpdate() {
        console.log('componentWillUpdate');
    }
    componentDidUpdate() {
        console.log('componentDidUpdate');
    }
    componentWillUnmount() {
        console.log('componentWillUnmount');
    }
    render() {
        return (
            <div className="form-group text-center">
                <form className="form-signin" method="post">
                    <img className="mb-4" src="https://static.thenounproject.com/png/17241-200.png" alt="The Creator" width="72" height="72" />
                    <h1 className="h3 mb-3 font-weight-normal">Please Enter Information</h1>
                    <input type="text" id="fUserId" name="fUserId" required="required" className="form-control" placeholder="Enter Account ID" />
                    <input type="pwssword" id="fPwd" name="fPwd" required="required" className="form-control" placeholder="Enter Account Password" />
                    <input type="text" id="fName" name="fName" required="required" className="form-control" placeholder="Enter Account Name" />
                    <input type="text" id="fEmail" name="fEmail" required="required" className="form-control" placeholder="Enter Account Email" />
                    <br></br>
                    <input type="submit" name="submit" className="btn btn-lg btn-primary btn-block" value="submit" />
                </form>
            </div>
        );
    }
}

ReactDOM.render(<Register />, document.getElementById('Register'));