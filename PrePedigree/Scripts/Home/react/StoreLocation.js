﻿import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
    

class App extends React.Component {
    componentDidMount() {
        var mobile_location = [
            { description: 'Taipei 101', latitude: '25.033612', longitude: '121.565008' },
            { description: 'NTU', latitude: '25.016858', longitude: '121.539689' },
            { description: 'Taipei Trains', latitude: '25.047931', longitude: '121.517072' },
            { description: 'Getac', latitude: '25.054794', longitude: '121.611458' }
        ];
        var markers = [];
        var marker;
        //var latLong = new google.maps.LatLng(25.054794, 121.611458);

        var options = {
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('map'), options);
        var bounds = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();

        var i = 0;

        for (i = 0; i < mobile_location.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(mobile_location[i].latitude, mobile_location[i].longitude),
                map: map
            });

            //extend the bounds to include each marker's position
            bounds.extend(marker.position);

            // click to show the description message
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(mobile_location[i].description);
                    infowindow.open(map, marker);
                }
            })(marker, i));

            markers.push(marker);
        }

        //now fit the map to the newly inclusive bounds
        map.fitBounds(bounds);

        //(optional) restore the zoom level after the map is done scaling
        var listener = google.maps.event.addListener(map, 'idle', function () {
            map.setZoom(12);
            google.maps.event.removeListener(listener);
        });

        for (i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
        }
    }

    render() {
        return (
            <div id="map" style={{ width: 800, height: 600 }}></div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('react'))