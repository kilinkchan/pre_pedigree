﻿import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import '../../../css/base.css';
const Title = () => (
    <Fragment>
        <h2>Pedigree Shop</h2>
    </Fragment>
);
ReactDOM.render(<Title />, document.getElementById('title'));
class PedigreeList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Pedigree: []
        }
    }
    componentWillMount() {
        fetch(this.props.url)
            .then(response =>
                response.json()
            )
            .then((data) =>
                this.setState({
                    Pedigree: data
                }))
            .catch(error => console.log(error))
    }
    render() {
        let PedigreeList = this.state.Pedigree;
        if (PedigreeList.length > 0) {
            const pedigree = PedigreeList.map((item) => {
                return (
                    <tbody key={item.fId}>
                        <tr>
                            <td> {item.fOrderGuid} </td>
                            <td> {item.fUserId} </td>
                            <td> {item.fPId} </td>
                            <td> {item.fName} </td>
                            <td> {item.fPrice} </td>
                            <td> {item.fQty} </td>
                            <td> {item.fIsApproved} </td>
                            <td><a href={'/home/DeletePedigree?fId=' + item.fId} className="btn btn-danger">delete</a> </td>
                        </tr>
                    </tbody>
                );
            })

            return (
                <div>
                    <div className="title-1">PedigreeShop</div>
                    <table className="table col-lg-12">
                        <thead>
                            <tr className="title-3">
                                <th>Order Id</th>
                                <th>User Id</th>
                                <th>P_Id</th>
                                <th>P_Name</th>
                                <th>P_Price</th>
                                <th>P_Quantity</th>
                                <th>Is Approved?</th>
                                <th></th>
                            </tr>
                        </thead>
                        {pedigree}
                    </table>
                </div>
            );
        } else {
            return (
                <div>Data is null</div>
            )
        }
    }
}
ReactDOM.render(<PedigreeList url="getPedigreeList" />, document.getElementById('PedigreeList'));

class PedigreeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fReceiver: "",
            fEmail: "",
            fAddress: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        const id = event.target.id;
        const value = event.target.value;
        this.setState({
            [id]: value
        });
    }

    handleSubmit(event) {
        const init = {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                fReceiver: this.state.fReceiver,
                fEmail: this.state.fEmail,
                fAddress: this.state.fAddress
            })
        }
        fetch(
            this.props.url,
            init
        ).then(res => res.json())
            .then(data => {
                console.log("data==>", data)
            })
            .catch(e => {
                console.log("err=>", e)
            })
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} method="post">
                <div className="form-horizontal">
                    <div className="title-1">Input Pedigree information</div>
                    <hr />
                    <div className="form-group">
                        <span className="title-2 control-label col-md-4">Receiver name</span>
                        <div className="col-md-8">
                            <input type="text" id="fReceiver" name="fReceiver" required="required" className="form-control" onChange={this.handleChange} />
                        </div>
                    </div>

                    <div className="form-group">
                        <span className="title-2 control-label col-md-4">Receiver Email</span>
                        <div className="col-md-8">
                            <input type="email" id="fEmail" name="fEmail" required="required" className="form-control" onChange={this.handleChange} />
                        </div>
                    </div>

                    <div className="form-group">
                        <span className="title-2 control-label col-md-4">Receiver Address</span>
                        <div className="col-md-8">
                            <input type="text" id="fAddress" name="fAddress" required="required" className="form-control" onChange={this.handleChange} />
                        </div>
                    </div>

                    <div className="form-group">
                        <div className="col-md-offset-4 col-md-12">
                            <input type="submit" value="Send" className="btn btn-lg btn-primary btn-block" />
                        </div>
                    </div>
                </div>
            </form>
        )
    }

}
ReactDOM.render(<PedigreeForm url="PedigreeShop" />, document.getElementById('PedigreeForm'))