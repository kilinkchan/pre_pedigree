﻿import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import '../../../css/base.css';
const Title = () => (
    <Fragment>
        <h1>Order List</h1>
    </Fragment>
);
ReactDOM.render(<Title />, document.getElementById('title'));

class OrderList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            orderList: [],
        }
    }
    componentWillMount() {
        fetch(this.props.url)
            .then(response =>
                response.json()
            )
            .then((data) =>
                this.setState({
                    orderList: data
                }))
            .catch(error => console.log(error))
    }

    render() {
        let orderList = this.state.orderList;       
        if (orderList.length > 0) {
            const order = orderList.map((item) => {
                var date = new Date(Number(item.fDate.substring(6, item.fDate.length - 2))).toLocaleString();
                return (
                    <tbody key={item.fOrderGuid}>
                        <tr>
                            <td>{item.fOrderGuid}</td>
                            <td>{item.fUserId}</td>
                            <td>{item.fReceiver}</td>
                            <td>{item.fEmail}</td>
                            <td>{item.fAddress}</td>
                            <td>{date}</td>
                        </tr>
                    </tbody>
                );
            })
            return (
                <table className="table col-lg-12">
                    <thead>
                        <tr className="title-2">
                            <th>OrderGuid</th>
                            <th>UserId</th>
                            <th>Receiver</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead >
                    {order}
                </table>
            )
        } else {
            return (
                <div> OrderList is null.</div>
            )
        }
    }
}

ReactDOM.render(<OrderList url="getOrderList" />, document.getElementById("OrderList"))