﻿
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import '../../../css/base.css';
const Title = () => (
    <Fragment>
        <div className="title">Product List</div>
    </Fragment>
);
ReactDOM.render(<Title />, document.getElementById('title'));

class objProduct {
    fId = 0;
    fPId = -1;
    fName = "None";
    fPrice = "None";
    fImg = '../../../images/z710.jpg';
}
class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            productList: new objProduct(),
            isLogin:"false"
        }
    }
    componentWillMount() {
        fetch(this.props.url)
            .then(response =>
                response.json()
            )
            .then((data) =>
                this.setState({
                    productList: data
                }))
            .catch(error => console.log(error))

        fetch("/Home/IsLogin")
            .then(response =>
                response.json()
            )
            .then((data) => {
                this.setState({
                    isLogin: data
                })                
            }
        )
            .catch(error => console.log(error))        
    }
    render() {
        let productList = this.state.productList;

        if (productList.length > 0) {
            return productList.map((product) => {
                return (
                    <div key={product.fId} className="col-lg-4">
                        <div className="thumbnail">
                            <img src={'../../../images/' + product.fImg} />
                            <div className="caption">
                                <div className="p-name">{product.fName}</div>
                                <p className="p-price">Price: {product.fPrice}</p>
                                {this.state.isLogin == "true" &&
                                        <p><a href={'home/AddtoPedigree?fPId=' + product.fPId } className="btn-primary">Add to Pedigree list</a></p>
                                }
                            </div>

                        </div>
                      
                    </div>
                );
            }
            )
        } else {
            return (
                <div>Data is null</div>
            )
        }
    }
}

ReactDOM.render(<Index url="home/getProductList" />, document.getElementById('index'));



