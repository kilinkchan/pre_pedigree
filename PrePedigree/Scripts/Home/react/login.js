﻿import React from 'react';
//import { render } from 'react-dom';
import ReactDOM from 'react-dom';
import '../../../css/base.css';
class LoginName extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentWillMount() {
        console.log('componentWillMount');
    }
    componentDidMount() {

        console.log('componentDidMount');
    }
    componentWillReceiveProps() {
        console.log('componentWillReceiveProps');
    }
    componentWillUpdate() {
        console.log('componentWillUpdate');
    }
    componentDidUpdate() {
        console.log('componentDidUpdate');
    }
    componentWillUnmount() {
        console.log('componentWillUnmount');
    }
    render() {
        return (
            <div className="form-group text-center">
                <form className="form-signin" method="post">
                    <img className="mb-4" src="https://static.thenounproject.com/png/17241-200.png" alt="The Creator" width="72" height="72" />
                    <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                    <input type="text" id="fUserId" name="fUserId" required="required" className="form-control" placeholder="Enter Account ID" />
                    <input type="pwssword" id="fPwd" name="fPwd" required="required" className="form-control" placeholder="Enter Account Password" />
                    <br></br>
                    <input type="submit" name="submit" className="btn btn-lg btn-primary btn-block" value="submit" />
                </form>
            </div>
        );
    }
}

ReactDOM.render(<LoginName />, document.getElementById('loginName'));