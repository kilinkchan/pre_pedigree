﻿import React from 'react';
import { render } from 'react-dom';
import styles from '../../../css/main.css';
//import '../../../css/main.css';
//import '../../../css/base.css';
class Layout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentWillMount() {
        console.log('componentWillMount');
    }
    componentDidMount() {

        console.log('componentDidMount');
    }
    componentWillReceiveProps() {
        console.log('componentWillReceiveProps');
    }
    componentWillUpdate() {
        console.log('componentWillUpdate');
    }
    componentDidUpdate() {
        console.log('componentDidUpdate');
    }
    componentWillUnmount() {
        console.log('componentWillUnmount');
    }
    render() {
        return (
            <div>
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <a className="navbar-brand" href={urlIndex}>Pedigree Shop</a>
                        </div>
                        <div className="collapse navbar-collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li><a href={urlIndex}>ProductList</a></li>
                                <li><a href={urlStoreLocation}>Store Location</a></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <li><a href={urlRegister}><span className="glyphicon glyphicon-user"></span>Register</a></li>
                                <li><a href={urlLogin}><span className="glyphicon glyphicon-log-in"></span> Login</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

render(<Layout />, document.getElementById('layout'));