using PrePedigree.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
namespace PrePedigree.Controllers
{
    public class HomeController : Controller
    {
        dbPrePedigreeEntities db = new dbPrePedigreeEntities();
        public ActionResult Index() {
            
            var products = db.tProduct.ToList();
            Debug.WriteLine("Index--->", products);
            if (Session["Member"] == null)
            {
                return View("Index", "_layout", products);
            }
            return View("Index", "_LayoutMember", products);
        }

        [HttpGet]
        public ActionResult getProductList()
        {
            return Json(db.tProduct.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PedigreeShop() {
            string fUserId = (Session["Member"] as tMember).fUserId;
            var orderDetails = db.tOrderDetail.Where(m => m.fUserId == fUserId && m.fIsApproved == "no").ToList();
            return View("PedigreeShop", "_LayoutMember");
        }

        [HttpGet]
        public ActionResult getPedigreeList()
        {
            string fUserId = (Session["Member"] as tMember).fUserId;
            var orderDetails = db.tOrderDetail.Where(m => m.fUserId == fUserId && m.fIsApproved == "no").ToList();
            if (orderDetails == null) {
                return View("PedigreeShop", "_LayoutMember");
            }
            return Json(orderDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddtoPedigree(string fPId)
        {
            string fUserId = (Session["Member"] as tMember).fUserId;
            var currentList = db.tOrderDetail.Where(m => m.fPId == fPId && m.fIsApproved == "no" && m.fUserId == fUserId).FirstOrDefault();
            if (currentList == null)
            {
                var product = db.tProduct.Where(m => m.fPId == fPId).FirstOrDefault();
                tOrderDetail orderDetail = new tOrderDetail();
                orderDetail.fUserId = fUserId;
                orderDetail.fPId = product.fPId;
                orderDetail.fName = product.fName;
                orderDetail.fPrice = product.fPrice;
                orderDetail.fQty = 1;
                orderDetail.fIsApproved = "no";
                db.tOrderDetail.Add(orderDetail);
            }
            else
            {
                currentList.fQty += 1;
            }
            db.SaveChanges();
            return RedirectToAction("PedigreeShop");
        }

        public ActionResult DeletePedigree(int fId) {
            var orderDetail = db.tOrderDetail.Where(m => m.fId == fId).FirstOrDefault();
            db.tOrderDetail.Remove(orderDetail);
            db.SaveChanges();
            return RedirectToAction("PedigreeShop");
        }

        [HttpPost]
        public ActionResult PedigreeShop(string fReceiver, string fEmail, string fAddress) {
            string fUserId = (Session["Member"] as tMember).fUserId;          
            string guid = Guid.NewGuid().ToString();
            tOrder order = new tOrder();
            order.fOrderGuid = guid;
            order.fUserId = fUserId;
            order.fReceiver = fReceiver;
            order.fEmail = fEmail;
            order.fAddress = fAddress;
            order.fDate = DateTime.Now;
            db.tOrder.Add(order);
            
            var pedigreeList = db.tOrderDetail
                .Where(m => m.fIsApproved == "no" && m.fUserId == fUserId)
                .ToList();
            foreach (var item in pedigreeList)
            {
                item.fOrderGuid = guid;
                item.fIsApproved = "yes";
            }
            db.SaveChanges();
            return RedirectToAction("OrderList");
        }

        public ActionResult OrderList()
        {
            string fUserId = (Session["Member"] as tMember).fUserId;
            var orders = db.tOrder.Where(m => m.fUserId == fUserId)
                .OrderByDescending(m => m.fDate).ToList();
            return View("OrderList", "_LayoutMember", orders);
        }

        [HttpGet]
        public ActionResult getOrderList() {
            return Json(db.tOrder.ToList(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Register()
        {
            return View();
        }
        //POST:Home/Register
        [HttpPost]
        public ActionResult Register(tMember pMember)
        {
            if (ModelState.IsValid == false)
            {
                return View();
            }
            var member = db.tMember.Where(m => m.fUserId == pMember.fUserId).FirstOrDefault();
            if (member == null)
            {
                db.tMember.Add(pMember);
                db.SaveChanges();
                return RedirectToAction("Login");
            }
            ViewBag.Message = "This account is exist. Register Fail";
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string fUserId, string fPwd)
        {
            Debug.WriteLine("fUserId: " + fUserId + "," + "fPwd: " + fPwd);
            var member = db.tMember.Where(m => m.fUserId == fUserId && m.fPwd == fPwd).FirstOrDefault();

            if (member == null)
            {
                ViewBag.Message = "Wrong!!! Login Error";
                return View();
            }

            Session["Welcome"] = member.fName + "Welcome!!!";
            Session["Member"] = member;

            //Debug.WriteLine("welcome"+Session["Welcome"]);

            return RedirectToAction("Index");
        }

        public ActionResult LoginName()
        {
            Debug.WriteLine("welcome" + Session["Welcome"]);
            return Json(Session["Welcome"], JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsLogin()
        {
            string isLogin = "false";

            if(Session["Welcome"] != null)
            {
                isLogin = "true";
            }

            Debug.WriteLine("isLogin: " + isLogin);
            return Json(isLogin, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            Debug.WriteLine("User logout");
            Session.Clear();
            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult StoreLocation()
        {
            //ViewBag.Message = "Your contact page.";
            if (Session["Member"] == null)
            {
                return View("StoreLocation", "_layout");
            }
            return View("StoreLocation", "_LayoutMember");
        }
    }
}
