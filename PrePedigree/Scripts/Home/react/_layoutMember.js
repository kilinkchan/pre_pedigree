﻿import React from 'react';
import { render } from 'react-dom';
import '../../../css/main.css';
class LayoutMember extends React.Component {
    constructor(props) {
        super(props);
        this.getName = this.getName.bind(this);
        this.state = {
            loginName:'',
        }
    }
    getName() {
        
    }
    componentWillMount() {
        fetch("/Home/LoginName")
            .then(response =>
                response.json()
            )
            .then((data) =>
                this.setState({
                    loginName: data
                })).catch(error => console.log(error))
        
    }
    componentDidMount() {
    }
    componentWillReceiveProps() {
    }
    componentWillUpdate() {
    }
    componentDidUpdate() {
    }
    componentWillUnmount() {
    }
    render() {        
        return (          
            <div>
                <nav className="navbar navbar-inverse navbar-fixed-top">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <a className="navbar-brand" href={urlIndex}>Pedigree Shop</a>
                        </div>
                        <div className="collapse navbar-collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li><a href={urlIndex}>ProductList</a></li>
                                <li><a href={urlStoreLocation}>StoreLocation</a></li>
                                <li className="dropdown">
                                    <a className="dropdown-toggle" data-toggle="dropdown" href="#">Order<span className="caret"></span></a>
                                    <ul className="dropdown-menu">
                                        <li><a href={urlPedigreeShop}>PedigreeShop</a></li>
                                        <li><a href={urlOrderList}>OrderList</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <li><a href={urlLogout}><span className="glyphicon glyphicon-log-out"></span>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

render(<LayoutMember />, document.getElementById('layout_member'));