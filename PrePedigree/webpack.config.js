"use strict";

var path = require("path");
var WebpackNotifierPlugin = require("webpack-notifier");
var BrowserSyncPlugin = require("browser-sync-webpack-plugin");

// https://sochix.ru/how-to-integrate-webpack-into-visual-studio-2015/
module.exports = {
    entry: {
        index: "./Scripts/Home/react/index.js",
        login: "./Scripts/Home/react/login.js",
        _layout: "./Scripts/Home/react/_layout.js",
        _layoutMember: "./Scripts/Home/react/_layoutMember.js",
        PedigreeShop: "./Scripts/Home/react/PedigreeShop.js",
        OrderList: "./Scripts/Home/react/OrderList.js",
        StoreLocation: "./Scripts/Home/react/StoreLocation.js",
        Register: "./Scripts/Home/react/Register.js",
        Footer: "./Scripts/Home/react/footer.js"
    },
    output: {
        path: path.join(__dirname, "./Scripts/dist/Home/react"),
        filename: "[name].js"
    },
    //devServer: {
    //    contentBase: ".",
    //    host: "localhost",
    //    port: 9000
    //},
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader"
                    }
                ]
            },
            {
                test: /\.(css|scss)$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" }
                ]
            }
        ]
    },
    // Enable Source Map
    // https://webpack.js.org/configuration/devtool/
    devtool: "inline-source-map",
    plugins: [new WebpackNotifierPlugin(), new BrowserSyncPlugin()]
};
